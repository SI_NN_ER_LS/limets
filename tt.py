import requests
from bs4 import BeautifulSoup
import json

# specify the query to search
query = 'loki'

# specify the url of the search results page
url = f'https://www.limetorrents.info/search/all/{query}/'

# send a GET request to the specified url
response = requests.get(url)

# parse the HTML content of the response using BeautifulSoup
soup = BeautifulSoup(response.content, 'html.parser')

# find all the rows in the search results table
rows = soup.find_all('tr', class_='tl')

# create an empty list to store the results
results = []

# iterate over the rows and extract the relevant information
for row in rows:
    # extract the movie name and link
    name_link = row.find('a', class_='tt-name')
    movie_name = name_link.get_text()
    movie_link = name_link['href']

    # extract the leechers and peers count
    leechers = row.find('td', class_='tdleech').get_text()
    peers = row.find('td', class_='tdseed').get_text()

    # extract the magnet link
    magnet_link = row.find('a', title='Download Magnet link')['href']

    # extract the uploaded date
    uploaded_date = row.find('td', class_='tdnormal').get_text()

    # add the information to the results list
    results.append({
        'movie_name': movie_name,
        'movie_link': movie_link,
        'leechers': leechers,
        'peers': peers,
        'magnet_link': magnet_link,
        'uploaded_date': uploaded_date
    })

# convert the results to JSON format and print it
print(json.dumps(results, indent=4))
